import razorpay
from django.core.mail.message import EmailMultiAlternatives
from django.template.loader import render_to_string


def send_email_for_razor_pay_payment(invoice, amount):
    """
    Sends email for razor pay payment.
    :param invoice: invoice object
    :param amount: amount to be paid
    :return: None
    """
    context = {}
    client = razorpay.Client(
        auth=(
            u"rzp_test_bW4FJpuKZaNaV4",
            u"siu2UI45ai7gtqbUhaoSCF6r",
        )
    )
    response = client.order.create(
        dict(
            amount=amount,
            currency=u'INR',
            receipt=u'For Payment',
            notes={},
            payment_capture=u'0'
        )
    )
    if response[u'status'] == u'created':
        context[u'order_id'] = response[u'id']
        context[u'name'] = invoice.name
        context[u'email'] = invoice.email
        context[u'phone'] = None
        context[u'price'] = amount
        context[u'product_id'] = invoice.id
        subject = render_to_string(u'emails/confirm_order_sub.txt', context)
        html_content = render_to_string(u'emails/confirm_order.html', context)
        text_content = render_to_string(u'emails/confirm_order.txt', context)

        msg = EmailMultiAlternatives(
            subject=subject, from_email=u'deepakbhardwaj017@gmail.com', to=[invoice.email],
            body=text_content
        )
        msg.attach_alternative(html_content, u"text/html")
        msg.send()
