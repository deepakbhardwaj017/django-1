# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from invoices import models as invoices_models

from django.contrib import admin

# Register your models here.

admin.site.register(invoices_models.Invoice, admin.ModelAdmin)
admin.site.register(invoices_models.InvoiceItem, admin.ModelAdmin)
