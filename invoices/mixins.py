# -*- coding: utf-8 -*-


class MultiSerializerClassViewSetMixin(object):
    """
    We can define multiple Serializer for a ViewSet using this Mixin.
    serializer_classes: takes a dict as input with actions defined as keys and the respective serializer to use as their values
    """
    serializer_classes = None

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action) if hasattr(self, u'action') else None
