# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import namedtuple

from django.db import models

from items import models as items_models


class Invoice(models.Model):
    STATUS = namedtuple(u'STATUS', [
        'DRAFT', 'PUBLISHED', 'CANCELLED'
    ])(
        DRAFT=1,
        PUBLISHED=2,
        CANCELLED=3
    )
    STATUS_CHOICES = (
        (STATUS.DRAFT, "Draft"),
        (STATUS.PUBLISHED, "Published"),
        (STATUS.CANCELLED, "Cancelled")
    )

    PAYMENT_MODE = namedtuple(u'PAYMENT_MODE', [
        'MANUAL', 'RAZOR_PAY'
    ])(
        MANUAL=1,
        RAZOR_PAY=2
    )
    PAYMENT_MODES_CHOICES = (
        (PAYMENT_MODE.MANUAL, "Manual"),
        (PAYMENT_MODE.RAZOR_PAY, "Razor Pay"),
    )

    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    items = models.ManyToManyField(items_models.Item, through=u'InvoiceItem', blank=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS.DRAFT)
    payment_mode = models.PositiveSmallIntegerField(choices=PAYMENT_MODES_CHOICES, default=PAYMENT_MODE.MANUAL)
    published_date_time = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Invoices'

    def __unicode__(self):
        return "{} - {}".format(self.name, self.email)


class InvoiceItem(models.Model):
    """
    To store a list of items per invoice
    """
    invoice = models.ForeignKey(Invoice)
    item = models.ForeignKey(items_models.Item)
    quantity = models.IntegerField()

    class Meta:
        verbose_name = "Invoice Item"
        verbose_name_plural = 'Invoice Items'
        unique_together = (u'invoice', u'item')

    def __unicode__(self):
        return '{} - {}'.format(self.invoice.name, self.item.name)
