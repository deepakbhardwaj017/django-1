from rest_framework.routers import SimpleRouter

from invoices import views as invoices_views

invoice_router = SimpleRouter()
invoice_router.register(
    prefix=u'invoices',
    viewset=invoices_views.InvoiceCreateEditRetrieveDeleteAPIViewSet,
    base_name=u'invoices'
)
urlpatterns = invoice_router.urls
