# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Prefetch
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import (
    response as rest_response,
    viewsets as rest_viewsets,
    permissions as rest_permissions,
    status as rest_status
)

from invoices import (
    models as invoices_models,
    serializers as invoices_serializers,
    mixins as invoices_mixins
)


class InvoiceCreateEditRetrieveDeleteAPIViewSet(
    invoices_mixins.MultiSerializerClassViewSetMixin,
    rest_viewsets.mixins.CreateModelMixin,
    rest_viewsets.mixins.RetrieveModelMixin,
    rest_viewsets.mixins.UpdateModelMixin,
    rest_viewsets.mixins.DestroyModelMixin,
    rest_viewsets.GenericViewSet
):
    """
    API to create/edit/detail Invoice:
    - Expected data format is
        {
            "name": "Deepak2222",
            "email": "deepakbhardwaj017@gmail.com",
            "invoice_items": [
                {"item": 1, "quantity": 10},
                {"item": 2, "quantity": 9}
            ],
            "status": 1,
            "payment_mode": 1
        }
    """
    http_method_names = [u'get', u'put', u'delete', u'post']
    serializer_classes = {
        u'retrieve': invoices_serializers.InvoiceDetailSerializer,
        u'update': invoices_serializers.InvoiceCreateUpdateSerializer,
        u'create': invoices_serializers.InvoiceCreateUpdateSerializer
    }
    model = invoices_models.Invoice
    permission_classes = (rest_permissions.AllowAny,)
    queryset = invoices_models.Invoice.objects.prefetch_related(
        Prefetch(
            u'invoiceitem_set',
            queryset=invoices_models.InvoiceItem.objects.select_related(u'item')
        )
    )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status != invoices_models.Invoice.STATUS.DRAFT:
            return rest_response.Response(data={'error': "You can delete invoice only in it's draft status."},
                                          status=rest_status.HTTP_403_FORBIDDEN)
        # Delete in voice items.
        instance.invoiceitems_set.delete()

        # Delete invoice.
        self.perform_destroy(instance)
        return rest_response.Response(data={}, status=rest_status.HTTP_204_NO_CONTENT)

    def get_serializer_context(self):
        context = super(InvoiceCreateEditRetrieveDeleteAPIViewSet, self).get_serializer_context()
        context[u'request'] = self.request
        return context


@csrf_exempt
def payment_status(request):

    response = request.POST

    context = {
        'razorpay_payment_id' : response['razorpay_payment_id'],
        'razorpay_order_id' : response['razorpay_order_id'],
        'razorpay_signature' : response['razorpay_signature']
    }
    return render(request, 'order_summary.html', context)
