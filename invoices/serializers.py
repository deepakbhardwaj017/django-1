from django.db import transaction
from django.db.models import Case, When, F, Sum
from rest_framework import (
    serializers as rest_serializers,
    fields as rest_fields,
    exceptions as rest_exceptions
)

from invoices import (
    models as invoice_models,
    utils as invoice_utils
)
from items import models as items_models


class InvoiceItemReadSerializer(rest_serializers.ModelSerializer):
    """
    Serializer to read invoice items.
    """
    id = rest_serializers.CharField(source=u'item.id')
    name = rest_serializers.CharField(source=u'item.name')
    description = rest_serializers.CharField(source=u'item.description')

    class Meta:
        model = invoice_models.InvoiceItem
        fields = (u'id', u'name', u'description', u'quantity')


class InvoiceDetailSerializer(rest_serializers.ModelSerializer):
    """
    Serializer to detail invoice.
    """
    items = InvoiceItemReadSerializer(source=u'invoiceitem_set.all', many=True)

    class Meta:
        model = invoice_models.Invoice
        fields = (u'id', u'name', u'email', u'items', u'status', u'payment_mode', u'published_date_time')


class InvoiceItemSerializer(rest_serializers.ModelSerializer):
    class Meta:
        model = invoice_models.InvoiceItem
        fields = (u'item', u'quantity',)


class InvoiceCreateUpdateSerializer(rest_serializers.ModelSerializer):
    """
    Serializer to create/edit invoice.
    """
    invoice_items = rest_fields.ListField(write_only=True, child=InvoiceItemSerializer(), required=True)

    @staticmethod
    def create_invoice_items(invoice_items_data, invoice):
        create_invoice_items_list = []
        item_ids = []
        for invoice_item in invoice_items_data:
            d = dict(invoice_item)
            create_invoice_items_list.append(
                invoice_models.InvoiceItem(
                    invoice=invoice, item=d[u'item'], quantity=d[u'quantity']
                )
            )
            item_ids.append(d[u'item'])
        invoice_models.InvoiceItem.objects.bulk_create(create_invoice_items_list)

        # calculate total price and return the same.
        price_aggregate = invoice_models.InvoiceItem.objects.filter(
            item_id__in=item_ids,
            invoice_id=invoice.id
        ).aggregate(
            gst=Sum(
                Case(
                    When(
                        item__gst_slab=items_models.Item.SLAB.SLAB_1,
                        then=F(u'item__price') * 0.05 * F(u'quantity')
                    ),
                    When(
                        item__gst_slab=items_models.Item.SLAB.SLAB_2,
                        then=F(u'item__price') * 0.12 * F(u'quantity')
                    ),
                    When(
                        item__gst_slab=items_models.Item.SLAB.SLAB_3,
                        then=F(u'item__price') * 0.18 * F(u'quantity')
                    ),
                    default=0
                )
            ),
            price=Sum(u'item__price') * F(u'quantity')
        )
        return price_aggregate[u'price'] + price_aggregate[u'gst']

    @transaction.atomic()
    def create(self, validated_data):
        invoice_items_data = validated_data.pop(u'invoice_items')
        invoice = super(InvoiceCreateUpdateSerializer, self).create(validated_data)
        amount = self.create_invoice_items(
            invoice_items_data=invoice_items_data,
            invoice=invoice
        )

        if (
            invoice.status == invoice_models.Invoice.STATUS.PUBLISHED
            and invoice.payment_mode == invoice_models.Invoice.PAYMENT_MODE.RAZOR_PAY
        ):
            # send email to the concerned person.
            invoice_utils.send_email_for_razor_pay_payment(
                invoice=invoice,
                amount=amount*100  # convert paisas to Rs
            )
        return invoice

    @transaction.atomic()
    def update(self, instance, validated_data):
        if validated_data[u'status'] != invoice_models.Invoice.STATUS.DRAFT:
            rest_exceptions.ValidationError(u'You can not update a published/canceled invoice')
        invoice_items_data = validated_data.pop(u'invoice_items')
        invoice_models.InvoiceItem.objects.filter(invoice=instance).delete()
        amount = self.create_invoice_items(
            invoice_items_data=invoice_items_data,
            invoice=instance
        )
        super(InvoiceCreateUpdateSerializer, self).update(instance, validated_data)

        # if status is published. Make the payment
        if (
                instance.status == invoice_models.Invoice.STATUS.PUBLISHED
                and instance.payment_mode == invoice_models.Invoice.PAYMENT_MODE.RAZOR_PAY
        ):
            # send email to the concerned person.
            invoice_utils.send_email_for_razor_pay_payment(
                invoice=instance,
                amount=amount
            )

    class Meta:
        model = invoice_models.Invoice
        fields = (u'name', u'email', u'invoice_items', u'status', u'payment_mode')
