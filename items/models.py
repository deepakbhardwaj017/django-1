# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import namedtuple

from django.db import models


class Item(models.Model):
    SLAB = namedtuple(u'SLAB', [
        'EXEMPTED', 'SLAB_1', 'SLAB_2', 'SLAB_3'
    ])(
        EXEMPTED=0,
        SLAB_1=5,
        SLAB_2=12,
        SLAB_3=18
    )
    SLAB_CHOICES = (
        (SLAB.EXEMPTED, "Exempted"),
        (SLAB.SLAB_1, "Slab 1"),
        (SLAB.SLAB_2, "Slab 2"),
        (SLAB.SLAB_3, "Slab 3")
    )
    
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=6000, blank=True)
    price = models.IntegerField()
    gst_slab = models.PositiveSmallIntegerField(choices=SLAB_CHOICES, default=SLAB.EXEMPTED)

    class Meta:
        verbose_name_plural = 'Items'
        indexes = [models.Index(fields=['name',])]

    def __unicode__(self):
        return self.name
