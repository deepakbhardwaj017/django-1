# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from items import models as items_models

from django.contrib import admin

# Register your models here.

admin.site.register(items_models.Item)
